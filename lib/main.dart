import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sample_app/SizeConfig.dart';
import 'package:sample_app/home.dart';
import 'package:sample_app/login_screen.dart';
import 'package:sample_app/profilefirst.dart';
import 'package:sample_app/profilesecond.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'HomeScreen App',
              // home: ProfileHome(),
              home: LoginScreen(),
            );
          },
        );
      },
    );
  }
}

class ProfileHome extends StatefulWidget {
  ProfileHome({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ProfileHomeState createState() => _ProfileHomeState();
}

class _ProfileHomeState extends State<ProfileHome>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    Home(),
    ProfileSecond(),
    ProfileFirst(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: Container(
        height: 10 * SizeConfig.heightMultiplier,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
        ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: GNav(
                gap: 10,
                activeColor: Colors.white,
                iconSize: 20,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                duration: Duration(milliseconds: 800),
                tabBackgroundColor: Colors.blue,
                tabs: [
                  GButton(
                    icon: LineIcons.home,
                    text: 'Home',
                    textStyle: TextStyle(
                      fontFamily: 'JosefinSans',
                      color: Colors.white,
                    ),
                  ),
                  GButton(
                    icon: LineIcons.youtube_play,
                    text: 'Youtube',
                    textStyle: TextStyle(
                      fontFamily: 'JosefinSans',
                      color: Colors.white,
                    ),
                  ),
                  GButton(
                    icon: LineIcons.user,
                    text: 'Profile',
                    textStyle: TextStyle(
                      fontFamily: 'JosefinSans',
                      color: Colors.white,
                    ),
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
